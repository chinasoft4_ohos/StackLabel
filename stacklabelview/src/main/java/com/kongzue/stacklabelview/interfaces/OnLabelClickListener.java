package com.kongzue.stacklabelview.interfaces;

import ohos.agp.components.Component;

/**
 * Author: @Kongzue
 * Github: https://github.com/kongzue/
 * Homepage: http://kongzue.com/
 * Mail: myzcxhh@live.cn
 *
 * @since 2018/10/24 22:11
 */
public interface OnLabelClickListener {
    /**
     * 点击事件
     *
     * @param index index
     * @param component component
     * @param string string
     */
    void onClick(int index, Component component, String string);
}
