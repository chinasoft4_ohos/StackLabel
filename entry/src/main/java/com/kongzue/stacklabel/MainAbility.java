package com.kongzue.stacklabel;

import com.kongzue.stacklabel.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;

/**
 * 主页面
 *
 * @since 2021-07-20
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        getWindow().setStatusBarColor(Color.getIntColor("#00574B"));
    }
}
