/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kongzue.stacklabel.slice;

import com.kongzue.stacklabel.ResourceTable;
import com.kongzue.stacklabelview.StackLabel;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

/**
 * MainAbilitySlice
 *
 * @since 2021-07-20
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final HiLogLabel HILOGLABEL = new HiLogLabel(HiLog.LOG_APP, 0x222, "my_app");
    private StackLabel stackLabelView;
    private Switch switchDelete;
    private TextField editMaxNum;
    private Switch switchSelect;
    private TextField editAdd;
    private Button btnAdd;
    private Text deleteText;
    private Text selectText;

    private List<String> labels = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        stackLabelView = (StackLabel) findComponentById(ResourceTable.Id_stackLabelView);
        switchDelete = (Switch) findComponentById(ResourceTable.Id_switchDelete);
        editMaxNum = (TextField) findComponentById(ResourceTable.Id_edit_maxNum);
        switchSelect = (Switch) findComponentById(ResourceTable.Id_switchSelect);
        editAdd = (TextField) findComponentById(ResourceTable.Id_edit_add);
        btnAdd = (Button) findComponentById(ResourceTable.Id_btn_add);
        deleteText = (Text) findComponentById(ResourceTable.Id_delete_text);
        selectText = (Text) findComponentById(ResourceTable.Id_select_text);
        labels = new ArrayList<>();
        labels.add("花哪儿记账");
        labels.add("给未来写封信");
        labels.add("密码键盘");
        labels.add("抬手唤醒");
        labels.add("Cutisan");
        labels.add("记-专注创作");
        labels.add("我也不知道我是谁");
        labels.add("崩崩崩");
        labels.add("Ohos");
        labels.add("开发");

        stackLabelView.setLabels(labels);

        setListener();
        setListener2();

        setCursorElement();
        initThumbElement();
    }

    private void setListener() {
        stackLabelView.setOnLabelClickListener((index, v, s) -> {
            if (switchDelete.isChecked()) {
                labels.remove(index);
                stackLabelView.setLabels(labels);
            } else {
                Toast.show(MainAbilitySlice.this, "点击了：" + s);
                if (stackLabelView.isSelectMode()) {
                    for (int i1 : stackLabelView.getSelectIndexList()) {
                        HiLog.info(HILOGLABEL, ">>>", "select: " + i1);
                    }
                }
            }
        });

        deleteText.setClickedListener(component -> {
            switchDelete.setCheckedStateChangedListener((absButton, b) -> stackLabelView.setDeleteButton(b));
            switchDelete.setChecked(!switchDelete.isChecked());
        });
        switchDelete.setCheckedStateChangedListener((absButton, b) -> stackLabelView.setDeleteButton(b));
        selectText.setClickedListener(component -> switchSelect.setChecked(!switchSelect.isChecked()));
        switchSelect.setCheckedStateChangedListener((absButton, b) -> {
            List<String> selectLabels = new ArrayList<>();
            selectLabels.add("Ohos");
            selectLabels.add("Cutisan");
            selectLabels.add("密码键盘");
            stackLabelView.setSelectMode(b, selectLabels);
        });
    }

    private void setListener2() {
        editMaxNum.addTextObserver((text, start, before, count) -> {
            StringBuilder builder = new StringBuilder();
            for (int index = 0; index < text.length(); index++) {
                String result = text.substring(index, index + 1);
                try {
                    Integer.parseInt(result);
                    builder.append(result);
                } catch (NumberFormatException e) {
                    text = text.replace(result, "");
                    HiLog.info(HILOGLABEL, "error-->" + e);
                }
            }
            if (!editMaxNum.getText().equals(text)) {
                editMaxNum.setText(text);
            }
            int num = 0;
            try {
                num = Integer.parseInt(text);
            } catch (NumberFormatException e) {
                HiLog.info(HILOGLABEL, "error-->" + e);
            }
            stackLabelView.setMaxSelectNum(num);
        });
        btnAdd.setClickedListener(component -> {
            String s1 = editAdd.getText().trim();
            if (!s1.isEmpty()) {
                labels.add(s1);
                stackLabelView.setLabels(labels);
            }
        });
    }

    private void setCursorElement() {
        // 设置光标颜色
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.LINE); // 设置背景类型
        shapeElement.setRgbColors(new RgbColor[]{RgbColor.fromArgbInt(Color.getIntColor("#D81B60")), RgbColor.fromArgbInt(Color.getIntColor("#D81B60"))}); // 设置颜色值 起始与结束时的颜色
        shapeElement.setGradientOrientation(ShapeElement.Orientation.TOP_TO_BOTTOM); // 设置渐变方向
        shapeElement.setStroke(50, RgbColor.fromArgbInt(Color.rgb(255, 0, 0))); // 设置光标高度
        editMaxNum.setCursorElement(shapeElement); // 设置光标背景色

        // 设置光标颜色
        ShapeElement shapeElementadd = new ShapeElement();
        shapeElementadd.setShape(ShapeElement.LINE); // 设置背景类型
        shapeElementadd.setRgbColors(new RgbColor[]{RgbColor.fromArgbInt(Color.getIntColor("#D81B60")), RgbColor.fromArgbInt(Color.getIntColor("#D81B60"))}); // 设置颜色值 起始与结束时的颜色
        shapeElementadd.setGradientOrientation(ShapeElement.Orientation.TOP_TO_BOTTOM); // 设置渐变方向
        shapeElementadd.setStroke(50, RgbColor.fromArgbInt(Color.rgb(255, 0, 0))); // 设置光标高度
        editAdd.setCursorElement(shapeElementadd); // 设置光标背景色
    }

    private void initThumbElement() {
        ShapeElement elementThumbOn = new ShapeElement();
        elementThumbOn.setShape(ShapeElement.OVAL);
        elementThumbOn.setRgbColor(RgbColor.fromArgbInt(0xFFFF4081));
        elementThumbOn.setCornerRadius(50);
        ShapeElement elementThumbOff = new ShapeElement();
        elementThumbOff.setShape(ShapeElement.OVAL);
        elementThumbOff.setRgbColor(RgbColor.fromArgbInt(0xFFFFFFFF));
        elementThumbOff.setCornerRadius(50);
        elementThumbOff.setBounds(0, 0, 60, 60);
        ShapeElement elementTrackOn = new ShapeElement();
        elementTrackOn.setShape(ShapeElement.RECTANGLE);
        elementTrackOn.setRgbColor(RgbColor.fromArgbInt(0xFFF6CEE3));
        elementTrackOn.setCornerRadius(50);
        ShapeElement elementTrackOff = new ShapeElement();
        elementTrackOff.setShape(ShapeElement.RECTANGLE);
        elementTrackOff.setRgbColor(RgbColor.fromArgbInt(0xFFBDBDBD));
        elementTrackOff.setCornerRadius(50);
        elementThumbOff.setBounds(0, 0, 50, 50);
        switchDelete.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
        switchDelete.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
        switchSelect.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
        switchSelect.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
    }

    private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }

    private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
