# StackLabel

## 项目介绍

- 项目名称: StackLabel
- 所属系列: openharmony的第三方组件适配移植
- 功能: 是堆叠标签组件，适合快速完成需要堆叠标签的场景，例如“搜索历史”、“猜你喜欢”等功能。
- 项目移植状态: 主功能完成
- 调用差异: 无
- 开发版本: sdk6，DevEco Studio 2.2 Beta2
- 基线版本：master分支


## 效果演示
<img src="img/demo1.gif"></img>


## 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:StackLabel:1.0.0')
    ......  
 }
```
在sdk6，DevEco Studio 2.2 Beta2下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明

1) 从XML布局文件创建：
```
 xmlns:app="http://schemas.huawei.com/hap/res-auto"
    <com.kongzue.stacklabelview.StackLabel
        ohos:id="$+id:stackLabelView"
        ohos:height="match_content"
        ohos:width="match_parent"
        app:deleteButton="false"
        app:deleteButtonImage="$media:img_delete"
        app:itemMargin="4vp"
        app:minSelectNum="2"
        app:paddingHorizontal="12vp"
        app:paddingVertical="8vp"
        app:selectTextColor="$color:colorAccent"
        app:textColor="#e6000000"
        app:textSize="12fp"
        />
    />
```

其中支持的自定义属性解释如下：

字段 | 含义 | 类型
---|---|---
app:textColor  | 标签文本颜色  | ColorInt
app:itemMargin  | 标签外边距  | int(像素)
app:itemMarginHorizontal | 标签外横向边距  | int(像素)
app:itemMarginVertical | 标签外纵向边距  | int(像素)
app:paddingHorizontal  | 标签内左右间距  | int(像素)
app:paddingVertical  | 标签内上下间距  | int(像素)
app:deleteButton  | 默认是否显示删除按钮  | boolean
app:textSize  | 标签文本字号  | int(像素)
app:deleteButtonImage  | 删除图标  | resId(资源id，例如@mipmap/img_delete)
app:labelBackground  | Label背景图  | resId(资源id，例如@mipmap/img_delete)
app:selectMode  | 选择模式开关  | boolean
app:selectBackground  | 选中的Label背景图  | resId(资源id，例如$graphic:rect_label_bkg_select_normal)
app:selectTextColor  | 选中标签文本颜色  | ColorInt
app:maxSelectNum  | 最大选择数量  | int
app:minSelectNum  | 最小选择数量  | int
app:maxLines | 最大可显示行数 | int


2) 添加内容：

StackLabel 目前仅支持纯文本标签表现，您可以将要显示的 String 字符串文本添加为 List 集合设置给 StackLabel，就会呈现想要的内容，范例如下：
```
labels = new ArrayList<>();
labels.add("花哪儿记账");
labels.add("给未来写封信");
labels.add("密码键盘");
labels.add("抬手唤醒");
labels.add("Cutisan");
labels.add("记-专注创作");
labels.add("我也不知道我是谁");
labels.add("崩崩崩");
labels.add("Ohos");
labels.add("开发");
stackLabelView.setLabels(labels);
```

也可以使用 String 集合创建：
```
stackLabelView.setLabels(new String[]{"花哪儿记账","给未来写封信","密码键盘","抬手唤醒"});
```

要实现标签点击，则需要设置点击监听器：
```
stackLabelView.setOnLabelClickListener(new OnLabelClickListener() {
      @Override
      public void onClick(int index, Component v, String s) {
       new ToastDialog(MainAbilitySlice.this).setText( "点击了：" + s).show();
    }
});
```
3) 删除模式

您可以在代码中使用 setDeleteButton(boolean) 控制 StackLabel 删除模式的开关：
```
stackLabelView.setDeleteButton(ture);
```

当 DeleteButton 开启时，点击任何标签即应删除该标签：
```
stackLabelView.setOnLabelClickListener(new OnLabelClickListener() {
      @Override
      public void onClick(int index, Component v, String s) {
        if (stackLabelView.isDeleteButton()) {      //是否开启了删除模式
            //删除并重新设置标签
            labels.remove(index);
            stackLabelView.setLabels(labels);
        } else {
            new ToastDialog(MainAbilitySlice.this).setText( "点击了：" + s).show();
        }
    }
});
```

另外也可以动态删除：
```
stackLabelView.remove(1);        //删除第1个索引的标签

stackLabelView.remove("标签2");   //删除名为“标签2”的标签
```

4) 选择模式

开启选择模式可以从 XML 布局中加入属性设置：
```
app:selectMode="true"
```
也可以从代码中开启：
```
stackLabelView.setSelectMode(true);
```

通过属性 maxSelectNum 可以设置最大可选数量，当值为 <=0 时不生效。

当属性 maxSelectNum = 1 时为单选模式，选择其他 Label 会自动取消之前选中的 Label。

当属性 maxSelectNum > 1 时为多选模式，选择数量大于 maxSelectNum 值时则无法选中更多的 Label。

maxSelectNum 也可在代码中通过 get/set 方法设置。

重复点击已选中的 Label 则会取消选中状态。

选中的角标集合可以通过以下方式获取：
```
stackLabelView.setOnLabelClickListener(new OnLabelClickListener() {
      @Override
      public void onClick(int index, Component v, String s) {
          if (stackLabelView.isSelectMode()) {
            for (int i : stackLabelView.getSelectIndexList()) {     //获取已选定集合的角标，也可通过stackLabelView.getSelectIndexArray()获取数组形式
                 HiLog.info(hiLogLabel,">>>", "select: " + i);
            }
        }
    }
});
```

另外可通过 setSelectMode(boolean, List<String>) 开启选择模式，并设置默认选中的标签；
```
List<String> selectLabels = new ArrayList<>();
selectLabels.add("Ohos");
selectLabels.add("Cutisan");
selectLabels.add("密码键盘");
stackLabelView.setSelectMode(isChecked, selectLabels);
```

以上，即 StackLabel 的基本使用流程。

5) StackLayout 使用方法
新增组件 StackLayout，它继承自 DependentLayout，可以将自定义的子布局直接放入即可实现堆叠排列。

使用方法：
```
 <com.kongzue.stacklabelview.StackLayout
        ohos:height="match_content"
        ohos:width="match_parent"
        app:itemMargin="4vp">
    
    <Text
        ohos:height="40vp"
        ohos:width="110vp"
        ohos:padding="10vp"
        ohos:text_alignment="center"
        ohos:background_element="#FFD4D4"
        ohos:text="文本1" />
    <Text
        ohos:width="120vp"
        ohos:height=="40vp"
        ohos:padding="10vp"
        ohos:text_alignment="center"
        ohos:background_element="#FDFFB8"
        ohos:text="文本2" />
        
    <!--子布局-->
        
</com.kongzue.stacklabelview.StackLayout>
```

目前仅支持 itemMargin 属性调整子布局间距。


## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
- 1.0.0

## 版权和许可信息
```
Copyright Kongzue StackLabel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```